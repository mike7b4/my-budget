mod args;
mod mybudget;
use args::{Action, Args};
use clap::Parser;
use log::{error, warn};
use mybudget::MyBudget;
use std::io::ErrorKind;
fn run_main() -> std::io::Result<()> {
    let args = Args::parse();
    let path = if let Some(filename) = &args.filename {
        filename.clone()
    } else {
        let mut path = dirs::data_dir().expect("Unsupported OS can not find data directory");
        path.push("my-budget/");
        std::fs::create_dir(&path).unwrap_or_else(|e| {
            if e.kind() != ErrorKind::AlreadyExists {
                warn!("Create: '{:?}' failed cause: {}", path, e);
            }
        });
        path
    };
    let mut myb = MyBudget::load(&path)?;
    match args.action {
        Action::AddAccount {
            name,
            kind,
            opening_balance,
        } => {
            myb.add_account(&name, &kind, opening_balance)?;
            myb.list_accounts(Some(kind))?;
        }
        Action::RenameAccount { from_name, to_name } => {
            myb.rename_account(&from_name, &to_name)?;
            myb.list_accounts(None)?;
        }
        Action::ListAccounts { kind } => {
            myb.list_accounts(kind)?;
        }
        Action::Add {
            from,
            to,
            comment,
            amount,
        } => {
            myb.add_transaction(&from, &to, &comment, amount)?;
        }
        Action::Remove { uid } => {
            myb.remove_transaction(uid)?;
        }
        Action::List {
            account_name,
            show_uid,
        } => {
            myb.list_transactions(&account_name, show_uid)?;
        }
    }

    Ok(())
}

fn main() -> std::io::Result<()> {
    simple_logger::init().unwrap();
    if let Err(e) = run_main() {
        error!("{}", e);
        std::process::exit(-1);
    }
    Ok(())
}
