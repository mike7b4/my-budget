use libbudget::accounts::{AccountKind, Accounts};
use libbudget::transactions::Transactions;
use nu_ansi_term::{Color::Blue, Color::Green, Color::Red, Style};
use std::io::{Error, ErrorKind};
use std::path::{Path, PathBuf};
type Result = std::io::Result<()>;
pub struct MyBudget {
    accounts: Accounts,
    transactions: Transactions,
    transactions_file: PathBuf,
    accounts_file: PathBuf,
}

impl MyBudget {
    pub fn load(path: &Path) -> std::io::Result<Self> {
        let mut accounts_file = path.to_path_buf();
        accounts_file.push("accounts.dat");
        let accounts = if std::fs::metadata(&accounts_file).is_ok() {
            Accounts::load(&accounts_file)?
        } else {
            Accounts::default()
        };

        let mut transactions_file = path.to_path_buf();
        transactions_file.push("transactions.dat");
        let transactions = if std::fs::metadata(&transactions_file).is_ok() {
            Transactions::load(&transactions_file)?
        } else {
            Transactions::default()
        };

        Ok(Self {
            accounts,
            transactions,
            accounts_file,
            transactions_file,
        })
    }

    pub fn list_transactions(&self, account_name: &str, show_uid: bool) -> Result {
        let uid = self.accounts.get_account_from_name(account_name)?.uid;
        println!(
            "{}",
            Style::new().bold().paint(format!(
                "                  List transactions for {}:",
                account_name
            ))
        );
        if show_uid {
            println!("╭─────┬───────────┬────────────────┬────────────────────────────────────────┬──────────╮");
            println!(
                "│{}│{}│{}│{}│{}│",
                Style::new().bold().paint("UID  "),
                Style::new().bold().paint("Date       "),
                Style::new().bold().paint("From account    "),
                Style::new()
                    .bold()
                    .paint("Comment                                 "),
                Style::new().bold().paint("Amount    ")
            );
            println!("├─────┼───────────┼────────────────┼────────────────────────────────────────┼──────────┤");
        } else {
            println!("╭───────────┬────────────────┬────────────────────────────────────────┬──────────╮");
            println!(
                "│{}│{}│{}│{}│",
                Style::new().bold().paint("Date       "),
                Style::new().bold().paint("From account    "),
                Style::new()
                    .bold()
                    .paint("Comment                                 "),
                Style::new().bold().paint("Amount    ")
            );
            println!("├───────────┼────────────────┼────────────────────────────────────────┼──────────┤");
        }
        fn print_transaction(
            accounts: &Accounts,
            uid: u16,
            show_uid: bool,
            t: &libbudget::transactions::Transaction,
        ) -> Result {
            let kind = accounts.kind(uid)?;
            let other_name;
            let amount = if t.from_uid == uid && kind != AccountKind::Income {
                other_name = accounts.get_name_from_uid(t.to_uid)?;
                Red.paint(format!(" -{:<8}", t.amount))
            } else {
                other_name = accounts.get_name_from_uid(t.from_uid)?;
                Green.paint(format!("  {:<8}", t.amount))
            };
            if show_uid {
                println!(
                    "│{}│{}│{:<16}│{}│{}│",
                    Style::new().bold().paint(format!("{:0>5}", t.uid)),
                    Blue.paint(t.date.format("%d %b %Y").to_string()),
                    other_name,
                    Style::new().paint(format!("{:<40}", t.comment)),
                    amount,
                );
            } else {
                println!(
                    "│{}│{:<16}│{}│{}│",
                    Blue.paint(t.date.format("%d %b %Y").to_string()),
                    other_name,
                    Style::new().paint(format!("{:<40}", t.comment)),
                    amount,
                );
            }
            Ok(())
        }

        let kind = self.accounts.kind(uid)?;
        self.transactions
            .list(&self.accounts, uid, show_uid, print_transaction)?;
        let amount = self.transactions.current_balance(uid, &kind)?;
        let comment;
        let amount = if amount < 0.0 {
            comment = "Balance";
            Red.paint(format!(" -{:<8.02}", amount))
        } else if kind == AccountKind::Expense {
            comment = "Total expense";
            Red.paint(format!("  {:<8.02}", amount))
        } else if kind == AccountKind::Income {
            comment = "Total income";
            Green.paint(format!("  {:<8.02}", amount))
        } else {
            comment = "Balance";
            Green.paint(format!("  {:<8.02}", amount))
        };
        if show_uid {
            let date = chrono::Utc::now();
            println!("├─────┼───────────┼────────────────┼────────────────────────────────────────┼──────────┤");
            println!(
                "│{}│{}│{:<16}│{}│{}│",
                Style::new().bold().paint(format!("{:0>5}", "")),
                Blue.paint(date.format("%d %b %Y").to_string()),
                account_name,
                Style::new().paint(format!("{:<40}", comment)),
                amount,
            );
            println!("╰─────┴───────────┴────────────────┴────────────────────────────────────────┴──────────╯");
        } else {
            let date = chrono::Utc::now();
            println!("├───────────┼────────────────┼────────────────────────────────────────┼──────────┤");
            println!(
                "│{}│{:<16}│{}│{}│",
                Blue.paint(date.format("%d %b %Y").to_string()),
                account_name,
                Style::new().paint(format!("{:<40}", comment)),
                amount,
            );
            println!(
            "╰───────────┴────────────────┴────────────────────────────────────────┴──────────╯"
        );
        }
        Ok(())
    }

    pub fn list_accounts(&self, kind: Option<AccountKind>) -> Result {
        fn header(kind: &AccountKind) {
            println!(
                "{}",
                Style::new()
                    .bold()
                    .paint(format!("            {:?} accounts", &kind))
            );
            println!("╭────────────┬────────────────────┬──────────╮");
            println!("│Date        │Name                │Balance   │");
            println!("├────────────┼────────────────────┼──────────┤");
        }
        if let Some(ref kind) = kind {
            header(kind);
            self.accounts
                .list_kind(kind, |account| println!("{}", account))?;
            println!("╰────────────┴────────────────────┴──────────╯");
        } else {
            header(&AccountKind::Income);
            self.accounts
                .list_kind(&AccountKind::Income, |account| println!("{}", account))?;
            println!("╰────────────┴────────────────────┴──────────╯");
            header(&AccountKind::Bank);
            self.accounts
                .list_kind(&AccountKind::Bank, |account| println!("{}", account))?;
            println!("╰────────────┴────────────────────┴──────────╯");
            header(&AccountKind::Expense);
            self.accounts
                .list_kind(&AccountKind::Expense, |account| println!("{}", account))?;
            println!("╰────────────┴────────────────────┴──────────╯");
        }
        Ok(())
    }

    pub fn add_transaction(&mut self, from: &str, to: &str, comment: &str, amount: f64) -> Result {
        self.transactions
            .add(&mut self.accounts, from, to, comment, amount)
    }

    pub fn remove_transaction(&mut self, uid: usize) -> Result {
        self.transactions.remove(&mut self.accounts, uid)
    }

    pub fn rename_account(&mut self, from_name: &str, to_name: &str) -> Result {
        self.accounts.rename(from_name, to_name)
    }

    pub fn add_account(
        &mut self,
        name: &str,
        kind: &AccountKind,
        opening_balance: Option<f64>,
    ) -> Result {
        if kind == &AccountKind::Bank && opening_balance.is_none() {
            return Err(Error::new(
                ErrorKind::Other,
                "You need to specify opening balance on bank accounts",
            ));
        }
        let uid = self.accounts.add(name, kind)?;
        if kind == &AccountKind::Bank {
            self.transactions.add_opening_balance(
                &mut self.accounts,
                uid,
                opening_balance.unwrap_or(0.0),
            )?;
        }
        Ok(())
    }
}

impl Drop for MyBudget {
    fn drop(&mut self) {
        self.accounts.save(&self.accounts_file).unwrap();
        self.transactions.save(&self.transactions_file).unwrap();
    }
}
