use clap::{Parser, Subcommand};
use libbudget::accounts::AccountKind;
use std::path::PathBuf;
#[derive(Debug, Subcommand)]
pub enum Action {
    /// Add a new account
    AddAccount {
        name: String,
        kind: AccountKind,
        opening_balance: Option<f64>,
    },
    /// List accounts
    ListAccounts { kind: Option<AccountKind> },
    /// Rename an account
    RenameAccount { from_name: String, to_name: String },
    /// Add transaction
    Add {
        from: String,
        to: String,
        comment: String,
        amount: f64,
    },
    /// Remove transaction
    Remove { uid: usize },
    /// List transactions for an account
    List {
        #[clap(short, long)]
        show_uid: bool,
        account_name: String,
    },
}

#[derive(Parser)]
pub struct Args {
    #[clap(long, short)]
    pub filename: Option<PathBuf>,
    #[clap(subcommand)]
    pub action: Action,
}
