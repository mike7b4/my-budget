pub fn capitalize_first_letter(s: &str) -> String {
    s[0..1].to_uppercase() + &s[1..].to_lowercase()
}

#[cfg(test)]
mod tests {
    use super::capitalize_first_letter;

    #[test]
    fn test_first_letter() {
        assert_eq!("Apa", capitalize_first_letter("apa"));
        assert_eq!("Apa", capitalize_first_letter("aPA"));
    }
}
