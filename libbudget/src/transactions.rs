use crate::accounts::{AccountKind, Accounts};
use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};
use std::fmt;
use std::io::{Error, ErrorKind};
use std::io::{Read, Write};
use std::path::Path;
type Result = std::io::Result<()>;

#[derive(Serialize, Deserialize)]
pub struct Transaction {
    pub uid: usize,
    pub date: DateTime<Utc>,
    pub from_uid: u16,
    pub to_uid: u16,
    pub comment: String,
    pub amount: f64,
}

impl fmt::Display for Transaction {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "|{:<12}|{:<20}|{:<8.2}|",
            self.date.format("%d %b %Y"),
            self.comment,
            self.amount
        )
    }
}

#[derive(Default, Serialize, Deserialize)]
pub struct Transactions {
    inner: Vec<Transaction>,
    last_uid: usize,
    #[serde(skip)]
    changed: bool,
}

impl Transactions {
    pub fn add(
        &mut self,
        accounts: &mut Accounts,
        from: &str,
        to: &str,
        comment: &str,
        amount: f64,
    ) -> Result {
        let from = accounts.get_account_from_name(from)?;
        let to = accounts.get_account_from_name(to)?;
        if from.kind == AccountKind::Expense {
            return Err(Error::new(
                ErrorKind::Other,
                "Can not withdraw from expense account",
            ));
        } else if to == from {
            return Err(Error::new(
                ErrorKind::Other,
                "Can not make a transaction from same account as to.",
            ));
        }
        self.add_real(accounts, from.uid, to.uid, comment, amount)
    }

    pub fn remove(&mut self, accounts: &mut Accounts, uid: usize) -> Result {
        let (index, tr) = self
            .inner
            .iter()
            .enumerate() // Nifty, enumerate append the vec index for use later
            .find(|(_, v)| v.uid == uid)
            .ok_or_else(|| {
                Error::new(
                    ErrorKind::Other,
                    format!("Transaction with UID: {} does not exist.", uid),
                )
            })?;

        let from = tr.from_uid;
        let to = tr.to_uid;
        accounts.add_amount(from, tr.amount)?;
        accounts.add_amount(to, -tr.amount)?;
        self.inner.remove(index);
        self.changed = true;

        Ok(())
    }

    fn add_real(
        &mut self,
        accounts: &mut Accounts,
        from_uid: u16,
        to_uid: u16,
        comment: &str,
        amount: f64,
    ) -> Result {
        let last_uid = self.last_uid + 1;
        self.inner.push(Transaction {
            uid: last_uid,
            date: Utc::now(),
            from_uid,
            to_uid,
            comment: comment.into(),
            amount,
        });
        self.last_uid = last_uid;
        self.changed = true;
        accounts.add_amount(from_uid, -amount)?;
        accounts.add_amount(to_uid, amount)?;
        Ok(())
    }

    pub fn add_opening_balance(
        &mut self,
        accounts: &mut Accounts,
        uid: u16,
        amount: f64,
    ) -> Result {
        // Verify UID is existing
        let _ = accounts.get_name_from_uid(uid)?;
        self.add_real(accounts, 0xFFFF, uid, "Opening balance", amount)?;
        self.changed = true;
        Ok(())
    }

    pub fn current_balance(&self, uid: u16, kind: &AccountKind) -> std::io::Result<f64> {
        let mut balance = 0.0;
        for t in self
            .inner
            .iter()
            .filter(|v| v.from_uid == uid || v.to_uid == uid)
            .collect::<Vec<&Transaction>>()
        {
            if t.from_uid == uid {
                balance -= t.amount;
            } else {
                balance += t.amount;
            }
        }
        if kind == &AccountKind::Income {
            Ok(balance * -1.0)
        } else {
            Ok(balance)
        }
    }

    pub fn list<F>(&self, accounts: &Accounts, uid: u16, show_uid: bool, f: F) -> Result
    where
        F: Fn(&Accounts, u16, bool, &Transaction) -> Result,
    {
        for t in self
            .inner
            .iter()
            .filter(|v| v.from_uid == uid || v.to_uid == uid)
            .collect::<Vec<&Transaction>>()
        {
            f(accounts, uid, show_uid, t)?;
        }
        Ok(())
    }

    pub fn save<P: AsRef<Path>>(&mut self, filename: P) -> Result {
        if !self.changed {
            return Ok(());
        }
        let mut fs = std::fs::File::create(&filename)?;
        fs.write_all(serde_json::to_string(self).unwrap().as_bytes())?;
        self.changed = false;
        Ok(())
    }

    pub fn load<P: AsRef<Path>>(filename: P) -> std::io::Result<Self> {
        let mut fs = std::fs::File::open(&filename)?;
        let mut buf = String::default();
        fs.read_to_string(&mut buf)?;
        let mut transactions: Transactions = serde_json::from_str(&buf).map_err(|error| {
            Error::new(
                ErrorKind::Other,
                format!(
                    "Read '{}' failed cause: {error}",
                    filename.as_ref().display()
                ),
            )
        })?;
        transactions.changed = false;
        Ok(transactions)
    }
}
