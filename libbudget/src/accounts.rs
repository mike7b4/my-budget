use crate::utils::capitalize_first_letter;
use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};
use std::fmt;
use std::fmt::Write as w;
use std::io::{Error, ErrorKind};
use std::io::{Read, Write};
use std::path::Path;
use std::str::FromStr;
type Result = std::io::Result<()>;

#[derive(Clone, Serialize, Deserialize, PartialEq, Eq, Debug)]
pub enum AccountKind {
    Income,
    Expense,
    Bank,
    Balance,
}

impl FromStr for AccountKind {
    type Err = std::io::Error;

    fn from_str(input: &str) -> std::result::Result<AccountKind, Self::Err> {
        match input {
            "income" => Ok(Self::Income),
            "expense" => Ok(Self::Expense),
            "bank" => Ok(Self::Bank),
            _ => Err(Error::new(
                ErrorKind::Other,
                format!("Type {} is not supported", input),
            )),
        }
    }
}

#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub struct Account {
    pub uid: u16,
    invisible: bool,
    pub name: String,
    pub date: DateTime<Utc>,
    pub kind: AccountKind,
    pub balance: f64,
}

impl fmt::Display for Account {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "│{:<12}│{:<20}│{:>10.2}│",
            self.date.format("%d %b %Y"),
            self.name,
            self.balance
        )
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Accounts {
    inner: Vec<Account>,
    #[serde(skip)]
    changed: bool,
}

impl Default for Accounts {
    fn default() -> Self {
        Self {
            inner: vec![Account {
                uid: 0xFFFF,
                invisible: false,
                name: "Balance".into(),
                date: chrono::Utc::now(),
                kind: AccountKind::Balance,
                balance: 0.0,
            }],
            changed: true,
        }
    }
}

impl fmt::Display for Accounts {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut s = String::new();
        let _ = self
            .list_kind(&AccountKind::Income, |account| {
                let _ = writeln!(s, "{}", account).is_ok();
            })
            .is_ok();
        let _ = self
            .list_kind(&AccountKind::Bank, |account| {
                let _ = writeln!(s, "{}", account).is_ok();
            })
            .is_ok();
        let _ = self
            .list_kind(&AccountKind::Expense, |account| {
                let _ = writeln!(s, "{}", account).is_ok();
            })
            .is_ok();
        write!(f, "{}", s)
    }
}

impl Accounts {
    pub fn add(&mut self, name: &str, kind: &AccountKind) -> std::io::Result<u16> {
        if self.inner.len() > 0x0FFF {
            return Err(Error::new(
                ErrorKind::Other,
                format!(
                    "Account limit reached. Must be less than {} accounts.",
                    0x1000
                ),
            ));
        }
        if kind == &AccountKind::Balance {
            return Err(Error::new(
                ErrorKind::Other,
                "Can only have one balance account",
            ));
        }

        let offset = match &kind {
            AccountKind::Income => 0x1000,
            AccountKind::Bank => 0x2000,
            AccountKind::Expense => 0x4000,
            _ => panic!("Should not enter this branch"),
        };

        let name = capitalize_first_letter(name);
        if self.inner.iter().any(|v| v.name == name) {
            return Err(Error::new(
                ErrorKind::Other,
                format!("Account with name: '{}' already exists", name),
            ));
        }
        let uid = offset + self.inner.len() as u16;
        self.inner.push(Account {
            uid,
            invisible: false,
            name,
            kind: kind.clone(),
            date: chrono::Utc::now(),
            balance: 0.0,
        });
        self.changed = true;
        Ok(uid)
    }

    pub fn kind(&self, uid: u16) -> std::io::Result<AccountKind> {
        self.inner
            .iter()
            .find(|v| v.uid == uid)
            .map(|v| v.kind.clone())
            .ok_or_else(|| {
                Error::new(
                    ErrorKind::Other,
                    format!("There is no account with UID: {uid}."),
                )
            })
    }

    pub(crate) fn add_amount(&mut self, uid: u16, amount: f64) -> Result {
        self.inner
            .iter_mut()
            .find(|v| v.uid == uid)
            .map(|v| {
                v.balance += amount;
                v.date = chrono::Utc::now();
            })
            .ok_or_else(|| {
                Error::new(
                    ErrorKind::Other,
                    format!("There is no account with UID: {uid}."),
                )
            })?;
        self.changed = true;
        Ok(())
    }

    pub fn save<P: AsRef<Path>>(&mut self, filename: P) -> Result {
        if !self.changed {
            return Ok(());
        }
        let mut fs = std::fs::File::create(&filename)?;
        fs.write_all(serde_json::to_string(self).unwrap().as_bytes())?;
        self.changed = false;
        Ok(())
    }

    pub fn load<P: AsRef<Path>>(filename: P) -> std::io::Result<Self> {
        let mut fs = std::fs::File::open(&filename)?;
        let mut buf = String::default();
        fs.read_to_string(&mut buf)?;
        let mut accounts: Accounts = serde_json::from_str(&buf).map_err(|error| {
            Error::new(
                ErrorKind::Other,
                format!(
                    "Read '{}' failed cause: {error}",
                    filename.as_ref().display()
                ),
            )
        })?;
        accounts.changed = false;
        Ok(accounts)
    }

    pub fn list<F>(&self, kind: Option<AccountKind>, mut f: F) -> std::io::Result<()>
    where
        F: FnMut(&Account),
    {
        if let Some(kind) = kind {
            self.list_kind(&kind, f).ok();
        } else {
            self.list_kind(&AccountKind::Income, &mut f).ok();
            self.list_kind(&AccountKind::Bank, &mut f).ok();
            self.list_kind(&AccountKind::Expense, &mut f).ok();
        }

        Ok(())
    }

    pub fn rename(&mut self, from_name: &str, to_name: &str) -> std::io::Result<()> {
        let from_name = capitalize_first_letter(from_name);
        let to_name = capitalize_first_letter(to_name);
        if self.inner.iter().any(|v| v.name == to_name) {
            return Err(Error::new(
                ErrorKind::Other,
                format!("Account with name: '{to_name}' already exist."),
            ));
        }
        self.inner
            .iter_mut()
            .find(|v| v.name == from_name)
            .map(|v| v.name = to_name)
            .ok_or_else(|| {
                Error::new(
                    ErrorKind::Other,
                    format!("Account with name: '{from_name}' does not exist."),
                )
            })?;
        self.changed = true;
        Ok(())
    }

    pub fn list_kind<F>(&self, kind: &AccountKind, mut f: F) -> std::io::Result<()>
    where
        F: FnMut(&Account),
    {
        for account in self
            .inner
            .iter()
            .filter(|v| v.kind == *kind)
            .collect::<Vec<&Account>>()
        {
            f(account);
        }
        Ok(())
    }

    pub fn get_name_from_uid(&self, uid: u16) -> std::io::Result<String> {
        self.inner
            .iter()
            .find(|v| v.uid == uid)
            .map(|a| a.name.clone())
            .ok_or_else(|| {
                Error::new(
                    ErrorKind::Other,
                    format!("Account with uid: {uid} does not exist."),
                )
            })
    }

    pub fn get_account_from_name(&self, name: &str) -> std::io::Result<&Account> {
        let name = capitalize_first_letter(name);
        self.inner.iter().find(|v| v.name == name).ok_or_else(|| {
            Error::new(
                ErrorKind::Other,
                format!("Account name: '{name}' does not exist."),
            )
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_accounts() {
        let accounts = Accounts::default();
        assert_eq!(1, accounts.inner.len());
    }

    #[test]
    fn test_dup_accounts() {
        let mut accounts = Accounts::default();
        assert_eq!(1, accounts.inner.len());
        accounts.add("foo", &AccountKind::Bank).unwrap();
        assert_eq!(2, accounts.inner.len());
        assert!(accounts.add("foo", &AccountKind::Expense).is_err());
        assert_eq!(2, accounts.inner.len());
    }
}
